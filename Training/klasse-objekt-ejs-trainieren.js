const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.set('views', 'Training')

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

// Wenn localhost:3000/klasse-objekt-ejs-trainieren aufgerufen wird ...

app.get('/klasse-objekt-ejs-trainieren',(req, res, next) => {   

    // ... wird klasse-objekt-ejs-trainieren.ejs gerendert:

    res.render('klasse-objekt-ejs-trainieren', {                                      
    })
})

// Eine Klasse ist Bauplan.
// Ein Objekt ist die konkrete Umsetzung auf der Grundlage des Bauplans.
// Alle Objekte eines Bauplans haben dieselben Eigenschaften, aber 
// möglicherweise unterschiedliche Eigenschaftswerte

// Klassendefinition 
// *****************

class Rechteck{
    constructor(){
        this.laenge
        this.breite

    }
}

// Deklaration eines Reckteck-Objekts vom Typ Rechteck
// Deklaration = Bekanntmachung, dass es ein Objekt vom Typ Rechteck geben soll.